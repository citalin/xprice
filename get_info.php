<?php
ini_set('memory_limit', '-1');

require_once'ress/mink/vendor/autoload.php';
require_once('ress/includes/include_dao.php');

use Behat\Mink\Mink,
Behat\Mink\Session,
Behat\Mink\Driver\GoutteDriver,
Behat\Mink\Driver\Goutte\Client as GoutteClient;

//Array the will be used as the Json output
$fullArr = array();

//start new transaction
$transaction = new Transaction();

//Get the value of the query
$query = urlencode($_GET["s"]);

$provider = $_GET["p"];

$site = DAOFactory::getBackendDAO()->load($provider);

if($site->base64)
{
    //Sears/Samborns requires that the query is coded in base64
    $query = base64_encode($query);
}

//Define the start url
$startUrl = $site->url.$query.$site->sujifoUrl;

//init Mink and register sessions
$mink = new Mink(array(
    'goutte1' => new Session(new GoutteDriver(new GoutteClient())),
    'goutte2' => new Session(new GoutteDriver(new GoutteClient()))
));

//set the default session name
$mink->setDefaultSessionName('goutte2');

//set the session variable
$session = $mink->getSession();
session_start();

//visit a page
$session->visit($startUrl);

//get the page
$page = $session->getPage();


// Check if the info exist
$test = $page->find('css',$site->find);
if($test != null){
    foreach ($page->findAll('css',$site->foreach) as $e) {
        $jArray = array();
        $product = $e->find('css',$site->product);
        if($product != null)
        {
            $jArray["product"] = $product->getText();
        }
        else
        {
            continue;
        }
        
        $price = $e->find('css',$site->precio);
        if($price != null)
        {
            $price = trim($price->getText());
        }else {
            $price ="-";
        }
        $jArray["price"] = $price;
        $jArray["url"] = $startUrl;
        array_push($fullArr,$jArray);
        if(count($fullArr) == 10)
            break;
    }
    echo json_encode($fullArr,JSON_UNESCAPED_UNICODE);
}
else
{
    echo "No results";
}

return;