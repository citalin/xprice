<?php
ini_set('memory_limit', '-1');

require_once'ress/mink/vendor/autoload.php';

use Behat\Mink\Mink,
    Behat\Mink\Session,
    Behat\Mink\Driver\GoutteDriver,
    Behat\Mink\Driver\Goutte\Client as GoutteClient;

    //Array the will be used as the Json output
    $fullArr = array();
    
    //Get the value of the query
    $query = urlencode($_GET["s"]);
    
    //Define the star url
    $startUrl = 'https://www.liverpool.com.mx/tienda?s='.$query;    
    
    //init Mink and register sessions
    $mink = new Mink(array(
        'goutte1' => new Session(new GoutteDriver(new GoutteClient())),
        'goutte2' => new Session(new GoutteDriver(new GoutteClient()))
    ));
    
    //set the default session name
    $mink->setDefaultSessionName('goutte2');
    
    //set the session variable
    $session = $mink->getSession();
    session_start();
    
    //visit a page
    $session->visit($startUrl);
    
    //get the page
    $page = $session->getPage();
    
    // Check if the info exist    
    $test = $page->find('css','.m-figureCard__figcaption');
    if($test != null){
        foreach ($page->findAll('css','.m-figureCard__figcaption') as $e) {        
            $jArray = array();
            $jArray["product"] = ($e->find('css','h5')->getText());
            $price = $e->find('css','.a-card-discount')->getText();           
            $jArray["price"] = (substr(trim($price),0,-2));
            $jArray["url"] = $startUrl;
            array_push($fullArr,$jArray);
            if(count($fullArr) == 10)
                break;
        }
        echo json_encode($fullArr,JSON_UNESCAPED_UNICODE);
    }
    else
    {
        echo "No results";
    }
    
return;

// The Library
include_once('includes/tools/simplehtmldom/simple_html_dom.php');

// get DOM from URL or file
$html = file_get_html($startUrl );


    // find all product titles 
    if($html!= null){
        if (count($html->find('.m-figureCard__figcaption')) != 0 ) {
            foreach ($html->find('.m-figureCard__figcaption') as $e) {
                $jArray = array();
                $jArray["product"] = ($e->find('h5', 0)->plaintext);
                $price = $e->find('.a-card-discount', 0);
                $jArray["price"] = (substr($price->plaintext,0,-2));

                array_push($fullArr,$jArray);
            }
            echo json_encode($fullArr,JSON_UNESCAPED_UNICODE);  
        }
    }
    else
    {
        echo "No results";            
    }
        
