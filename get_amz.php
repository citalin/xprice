<?php
ini_set('memory_limit', '-1');

require_once'ress/mink/vendor/autoload.php';

use Behat\Mink\Mink,
    Behat\Mink\Session,
    Behat\Mink\Driver\GoutteDriver,
    Behat\Mink\Driver\Goutte\Client as GoutteClient;

    //Array the will be used as the Json output
    $fullArr = array();
    
    //Get the value of the query
    $query = urlencode($_GET["s"]);
    
    //Define the star url
    $startUrl = 'https://www.amazon.com.mx/s?k='.$query;    
    
    //init Mink and register sessions
    $mink = new Mink(array(
        'goutte1' => new Session(new GoutteDriver(new GoutteClient())),
        'goutte2' => new Session(new GoutteDriver(new GoutteClient()))
    ));
    
    //set the default session name
    $mink->setDefaultSessionName('goutte2');
    
    //set the session variable
    $session = $mink->getSession();
    session_start();
    
    //visit a page
    $session->visit($startUrl);
    
    //get the page
    $page = $session->getPage();
    
    // Check if the info exist    
    $test = $page->find('css','.s-result-item');
    if($test != null){
        foreach ($page->findAll('css','.s-result-item') as $e) {            
            $jArray = array();
            $jArray["product"] = ($e->find('css','h2')->getText());
            $price = $e->find('css','.a-price-whole'); 
            if($price != null)
            {
                $price = trim($price->getText());
            }else {
                $price ="-";
            }
            $jArray["price"] = $price;
            $jArray["url"] = $startUrl;
            array_push($fullArr,$jArray);
            if(count($fullArr) == 10)
                break;
        }
        echo json_encode($fullArr,JSON_UNESCAPED_UNICODE);
    }
    else
    {
        echo "No results";
    }
    
return;