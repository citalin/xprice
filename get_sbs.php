<?php
ini_set('memory_limit', '-1');

require_once'ress/mink/vendor/autoload.php';

use Behat\Mink\Mink,
    Behat\Mink\Session,
    Behat\Mink\Driver\GoutteDriver,
    Behat\Mink\Driver\Goutte\Client as GoutteClient;

    //Array the will be used as the Json output
    $fullArr = array();
    
    //Get the value of the query
    $query = urlencode($_GET["s"]);
    
    //Sears requires that the query is coded in base64
    $query = base64_encode($query);
    
    //Define the star url
    $startUrl = 'https://www.sanborns.com.mx/buscador/'.$query."/1/";    
    
    //init Mink and register sessions
    $mink = new Mink(array(
        'goutte1' => new Session(new GoutteDriver(new GoutteClient())),
        'goutte2' => new Session(new GoutteDriver(new GoutteClient()))
    ));
    
    //set the default session name
    $mink->setDefaultSessionName('goutte2');
    
    //set the session variable
    $session = $mink->getSession();
    session_start();
    
    //visit a page
    $session->visit($startUrl);
    
    //get the page
    $page = $session->getPage();
    
    // Check if the info exist    
    $test = $page->find('css','.productbox');
    if($test != null){
        foreach ($page->findAll('css','.productbox') as $e) {            
            $jArray = array();            
            $jArray["product"] = $e->find('css','.descrip')->getText();
            $price = $e->find('css','.preciodesc');             
            if($price != null)
            {
                $price = trim($price->getText());
                $price = str_replace("Precio Internet:","",$price);
            }else {
                $price ="-";
            }
            $jArray["price"] = $price;
            $jArray["url"] = $startUrl;
            array_push($fullArr,$jArray);
            if(count($fullArr) == 10)
                break;
        }
        echo json_encode($fullArr,JSON_UNESCAPED_UNICODE);
    }
    else
    {
        echo "No results";
    }
    
return;