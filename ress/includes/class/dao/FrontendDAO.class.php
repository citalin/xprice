<?php
/**
 * Intreface DAO
 *
 * @author: http://phpdao.com
 * @date: 2019-10-27 00:09
 */
interface FrontendDAO{

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @Return Frontend 
	 */
	public function load($id);

	/**
	 * Get all records from table
	 */
	public function queryAll();
	
	/**
	 * Get all records from table ordered by field
	 * @Param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn);
	
	/**
 	 * Delete record from table
 	 * @param frontend primary key
 	 */
	public function delete($id);
	
	/**
 	 * Insert record to table
 	 *
 	 * @param Frontend frontend
 	 */
	public function insert($frontend);
	
	/**
 	 * Update record in table
 	 *
 	 * @param Frontend frontend
 	 */
	public function update($frontend);	

	/**
	 * Delete all rows
	 */
	public function clean();

	public function queryByNombre($value);

	public function queryByDescripcion($value);

	public function queryByImagen($value);

	public function queryByUrlPrincipal($value);


	public function deleteByNombre($value);

	public function deleteByDescripcion($value);

	public function deleteByImagen($value);

	public function deleteByUrlPrincipal($value);


}
?>