<?php
/**
 * Intreface DAO
 *
 * @author: http://phpdao.com
 * @date: 2019-10-27 00:09
 */
interface BackendDAO{

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @Return Backend 
	 */
	public function load($id);

	/**
	 * Get all records from table
	 */
	public function queryAll();
	
	/**
	 * Get all records from table ordered by field
	 * @Param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn);
	
	/**
 	 * Delete record from table
 	 * @param backend primary key
 	 */
	public function delete($id);
	
	/**
 	 * Insert record to table
 	 *
 	 * @param Backend backend
 	 */
	public function insert($backend);
	
	/**
 	 * Update record in table
 	 *
 	 * @param Backend backend
 	 */
	public function update($backend);	

	/**
	 * Delete all rows
	 */
	public function clean();

	public function queryByBase64($value);

	public function queryByUrl($value);

	public function queryBySujifoUrl($value);

	public function queryByFind($value);

	public function queryByForeach($value);

	public function queryByProduct($value);

	public function queryByPrecio($value);


	public function deleteByBase64($value);

	public function deleteByUrl($value);

	public function deleteBySujifoUrl($value);

	public function deleteByFind($value);

	public function deleteByForeach($value);

	public function deleteByProduct($value);

	public function deleteByPrecio($value);


}
?>