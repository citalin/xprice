<?php

/**
 * DAOFactory
 * @author: http://phpdao.com
 * @date: ${date}
 */
class DAOFactory{
	
	/**
	 * @return BackendDAO
	 */
	public static function getBackendDAO(){
		return new BackendMySqlExtDAO();
	}

	/**
	 * @return FrontendDAO
	 */
	public static function getFrontendDAO(){
		return new FrontendMySqlExtDAO();
	}


}
?>