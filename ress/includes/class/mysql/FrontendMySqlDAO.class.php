<?php
/**
 * Class that operate on table 'frontend'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2019-10-27 00:09
 */
class FrontendMySqlDAO implements FrontendDAO{

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @return FrontendMySql 
	 */
	public function load($id){
		$sql = 'SELECT * FROM frontend WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($id);
		return $this->getRow($sqlQuery);
	}

	/**
	 * Get all records from table
	 */
	public function queryAll(){
		$sql = 'SELECT * FROM frontend';
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
	 * Get all records from table ordered by field
	 *
	 * @param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn){
		$sql = 'SELECT * FROM frontend ORDER BY '.$orderColumn;
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
 	 * Delete record from table
 	 * @param frontend primary key
 	 */
	public function delete($id){
		$sql = 'DELETE FROM frontend WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($id);
		return $this->executeUpdate($sqlQuery);
	}
	
	/**
 	 * Insert record to table
 	 *
 	 * @param FrontendMySql frontend
 	 */
	public function insert($frontend){
		$sql = 'INSERT INTO frontend (nombre, descripcion, imagen, url_principal) VALUES (?, ?, ?, ?)';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->set($frontend->nombre);
		$sqlQuery->set($frontend->descripcion);
		$sqlQuery->set($frontend->imagen);
		$sqlQuery->set($frontend->urlPrincipal);

		$id = $this->executeInsert($sqlQuery);	
		$frontend->id = $id;
		return $id;
	}
	
	/**
 	 * Update record in table
 	 *
 	 * @param FrontendMySql frontend
 	 */
	public function update($frontend){
		$sql = 'UPDATE frontend SET nombre = ?, descripcion = ?, imagen = ?, url_principal = ? WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->set($frontend->nombre);
		$sqlQuery->set($frontend->descripcion);
		$sqlQuery->set($frontend->imagen);
		$sqlQuery->set($frontend->urlPrincipal);

		$sqlQuery->set($frontend->id);
		return $this->executeUpdate($sqlQuery);
	}

	/**
 	 * Delete all rows
 	 */
	public function clean(){
		$sql = 'DELETE FROM frontend';
		$sqlQuery = new SqlQuery($sql);
		return $this->executeUpdate($sqlQuery);
	}

	public function queryByNombre($value){
		$sql = 'SELECT * FROM frontend WHERE nombre = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}

	public function queryByDescripcion($value){
		$sql = 'SELECT * FROM frontend WHERE descripcion = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}

	public function queryByImagen($value){
		$sql = 'SELECT * FROM frontend WHERE imagen = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}

	public function queryByUrlPrincipal($value){
		$sql = 'SELECT * FROM frontend WHERE url_principal = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}


	public function deleteByNombre($value){
		$sql = 'DELETE FROM frontend WHERE nombre = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByDescripcion($value){
		$sql = 'DELETE FROM frontend WHERE descripcion = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByImagen($value){
		$sql = 'DELETE FROM frontend WHERE imagen = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByUrlPrincipal($value){
		$sql = 'DELETE FROM frontend WHERE url_principal = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}


	
	/**
	 * Read row
	 *
	 * @return FrontendMySql 
	 */
	protected function readRow($row){
		$frontend = new Frontend();
		
		$frontend->id = $row['id'];
		$frontend->nombre = $row['nombre'];
		$frontend->descripcion = $row['descripcion'];
		$frontend->imagen = $row['imagen'];
		$frontend->urlPrincipal = $row['url_principal'];

		return $frontend;
	}
	
	protected function getList($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		$ret = array();
		for($i=0;$i<count($tab);$i++){
			$ret[$i] = $this->readRow($tab[$i]);
		}
		return $ret;
	}
	
	/**
	 * Get row
	 *
	 * @return FrontendMySql 
	 */
	protected function getRow($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		if(count($tab)==0){
			return null;
		}
		return $this->readRow($tab[0]);		
	}
	
	/**
	 * Execute sql query
	 */
	protected function execute($sqlQuery){
		return QueryExecutor::execute($sqlQuery);
	}
	
		
	/**
	 * Execute sql query
	 */
	protected function executeUpdate($sqlQuery){
		return QueryExecutor::executeUpdate($sqlQuery);
	}

	/**
	 * Query for one row and one column
	 */
	protected function querySingleResult($sqlQuery){
		return QueryExecutor::queryForString($sqlQuery);
	}

	/**
	 * Insert row to table
	 */
	protected function executeInsert($sqlQuery){
		return QueryExecutor::executeInsert($sqlQuery);
	}
}
?>