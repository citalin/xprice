<?php
/**
 * Class that operate on table 'backend'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2019-10-27 00:09
 */
class BackendMySqlDAO implements BackendDAO{

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @return BackendMySql 
	 */
	public function load($id){
		$sql = 'SELECT * FROM backend WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($id);
		return $this->getRow($sqlQuery);
	}

	/**
	 * Get all records from table
	 */
	public function queryAll(){
		$sql = 'SELECT * FROM backend';
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
	 * Get all records from table ordered by field
	 *
	 * @param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn){
		$sql = 'SELECT * FROM backend ORDER BY '.$orderColumn;
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
 	 * Delete record from table
 	 * @param backend primary key
 	 */
	public function delete($id){
		$sql = 'DELETE FROM backend WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($id);
		return $this->executeUpdate($sqlQuery);
	}
	
	/**
 	 * Insert record to table
 	 *
 	 * @param BackendMySql backend
 	 */
	public function insert($backend){
		$sql = 'INSERT INTO backend (base64, url, sujifo_url, find, foreach, product, precio) VALUES (?, ?, ?, ?, ?, ?, ?)';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->setNumber($backend->base64);
		$sqlQuery->set($backend->url);
		$sqlQuery->set($backend->sujifoUrl);
		$sqlQuery->set($backend->find);
		$sqlQuery->set($backend->foreach);
		$sqlQuery->set($backend->product);
		$sqlQuery->set($backend->precio);

		$id = $this->executeInsert($sqlQuery);	
		$backend->id = $id;
		return $id;
	}
	
	/**
 	 * Update record in table
 	 *
 	 * @param BackendMySql backend
 	 */
	public function update($backend){
		$sql = 'UPDATE backend SET base64 = ?, url = ?, sujifo_url = ?, find = ?, foreach = ?, product = ?, precio = ? WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->setNumber($backend->base64);
		$sqlQuery->set($backend->url);
		$sqlQuery->set($backend->sujifoUrl);
		$sqlQuery->set($backend->find);
		$sqlQuery->set($backend->foreach);
		$sqlQuery->set($backend->product);
		$sqlQuery->set($backend->precio);

		$sqlQuery->set($backend->id);
		return $this->executeUpdate($sqlQuery);
	}

	/**
 	 * Delete all rows
 	 */
	public function clean(){
		$sql = 'DELETE FROM backend';
		$sqlQuery = new SqlQuery($sql);
		return $this->executeUpdate($sqlQuery);
	}

	public function queryByBase64($value){
		$sql = 'SELECT * FROM backend WHERE base64 = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->getList($sqlQuery);
	}

	public function queryByUrl($value){
		$sql = 'SELECT * FROM backend WHERE url = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}

	public function queryBySujifoUrl($value){
		$sql = 'SELECT * FROM backend WHERE sujifo_url = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}

	public function queryByFind($value){
		$sql = 'SELECT * FROM backend WHERE find = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}

	public function queryByForeach($value){
		$sql = 'SELECT * FROM backend WHERE foreach = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}

	public function queryByProduct($value){
		$sql = 'SELECT * FROM backend WHERE product = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}

	public function queryByPrecio($value){
		$sql = 'SELECT * FROM backend WHERE precio = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}


	public function deleteByBase64($value){
		$sql = 'DELETE FROM backend WHERE base64 = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByUrl($value){
		$sql = 'DELETE FROM backend WHERE url = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteBySujifoUrl($value){
		$sql = 'DELETE FROM backend WHERE sujifo_url = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByFind($value){
		$sql = 'DELETE FROM backend WHERE find = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByForeach($value){
		$sql = 'DELETE FROM backend WHERE foreach = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByProduct($value){
		$sql = 'DELETE FROM backend WHERE product = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByPrecio($value){
		$sql = 'DELETE FROM backend WHERE precio = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}


	
	/**
	 * Read row
	 *
	 * @return BackendMySql 
	 */
	protected function readRow($row){
		$backend = new Backend();
		
		$backend->id = $row['id'];
		$backend->base64 = $row['base64'];
		$backend->url = $row['url'];
		$backend->sujifoUrl = $row['sujifo_url'];
		$backend->find = $row['find'];
		$backend->foreach = $row['foreach'];
		$backend->product = $row['product'];
		$backend->precio = $row['precio'];

		return $backend;
	}
	
	protected function getList($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		$ret = array();
		for($i=0;$i<count($tab);$i++){
			$ret[$i] = $this->readRow($tab[$i]);
		}
		return $ret;
	}
	
	/**
	 * Get row
	 *
	 * @return BackendMySql 
	 */
	protected function getRow($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		if(count($tab)==0){
			return null;
		}
		return $this->readRow($tab[0]);		
	}
	
	/**
	 * Execute sql query
	 */
	protected function execute($sqlQuery){
		return QueryExecutor::execute($sqlQuery);
	}
	
		
	/**
	 * Execute sql query
	 */
	protected function executeUpdate($sqlQuery){
		return QueryExecutor::executeUpdate($sqlQuery);
	}

	/**
	 * Query for one row and one column
	 */
	protected function querySingleResult($sqlQuery){
		return QueryExecutor::queryForString($sqlQuery);
	}

	/**
	 * Insert row to table
	 */
	protected function executeInsert($sqlQuery){
		return QueryExecutor::executeInsert($sqlQuery);
	}
}
?>