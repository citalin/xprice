<?php
ini_set('memory_limit', '-1');

require_once'ress/mink/vendor/autoload.php';

use Behat\Mink\Mink,
    Behat\Mink\Session,
    Behat\Mink\Driver\GoutteDriver,
    Behat\Mink\Driver\Goutte\Client as GoutteClient;

    //Array the will be used as the Json output
    $fullArr = array();
    
    //Get the value of the query
    $query = urlencode($_GET["s"]);
    
    //Define the star url
    $startUrl = 'https://listado.mercadolibre.com.mx/'.$query;    
    
    //init Mink and register sessions
    $mink = new Mink(array(
        'goutte1' => new Session(new GoutteDriver(new GoutteClient())),
        'goutte2' => new Session(new GoutteDriver(new GoutteClient()))
    ));
    
    //set the default session name
    $mink->setDefaultSessionName('goutte2');
    
    //set the session variable
    $session = $mink->getSession();
    session_start();
    
    //visit a page
    $session->visit($startUrl);
    
    //get the page
    $page = $session->getPage();
    
    // Check if the info exist    
    $test = $page->find('css','.item__info');
    if($test != null){
        foreach ($page->findAll('css','.item__info') as $e) {        
            $jArray = array();
            $jArray["product"] = ($e->find('css','.main-title')->getText());
            $price = $e->find('css','.price__fraction')->getText();           
            $jArray["price"] = trim($price);
            $jArray["url"] = $startUrl;
            array_push($fullArr,$jArray);
            if(count($fullArr) == 10)
                break;
        }
        echo json_encode($fullArr,JSON_UNESCAPED_UNICODE);
    }
    else
    {
        echo "No results";
    }
    
return;